
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsServer.h>
#include <Hash.h>
#include <LiquidCrystal_I2C.h>

#include "secret.h"

#define PORT 7777																// port serveur websocket

#define LCD_LIGNE		2														// nombre de ligne du lcd
#define LCD_COLONNE		16														// nombre de colonne du lcd
#define LCD_ADRESSE		0x27													// adresse interne du lcd (0x27 ou 0x3F en fonction du model)

#define LCDSCL			D1														// i2c scl
#define LCDSDA			D2														// i2c sda

#define LEDRGBR			D5														// rouge
#define LEDRGBG			D6														// vert
#define LEDRGBB			D7														// bleu

WebSocketsServer webSocket = WebSocketsServer(PORT);
LiquidCrystal_I2C lcd(LCD_ADRESSE, LCD_COLONNE, LCD_LIGNE);						// adresse 0x27 ou 0x3f, 2 lignes de 16 caracteres

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght)
{
	//Serial.printf("[%u] Message recu: %s\r\n", num, payload);
	
	switch(type)
	{
		case WStype_DISCONNECTED:
			{
			}
			break;
			
		case WStype_CONNECTED:
			{ 
				IPAddress ip = webSocket.remoteIP(num);
				Serial.printf("[%u] Hote: %d.%d.%d.%d url: %s\r\n", num, ip[0], ip[1], ip[2], ip[3], payload);
			}
			break;

		case WStype_TEXT:
			{
				//Serial.printf("[%u] get Text: %s\r\n", num, payload);
				String _payload = String((char *) &payload[0]);
				//Serial.println(_payload);

				String idLed = (_payload.substring(0, 4));
				String intensity = (_payload.substring(_payload.indexOf(":")+1, _payload.length()));
				
				int intLed = intensity.toInt();
				updateLed(idLed, intLed);
			}
			break;     

		case WStype_BIN:
			{
				hexdump(payload, lenght);
				// echo data back to browser
				webSocket.sendBIN(num, payload, lenght);
			}
			break;
	}
}

void setup()
{
	Serial.begin(115200);
	
	pinMode(LEDRGBR, OUTPUT);
	pinMode(LEDRGBG, OUTPUT);
	pinMode(LEDRGBB, OUTPUT);
	
	analogWrite(LEDRGBR, 0);
	analogWrite(LEDRGBG, 0);
	analogWrite(LEDRGBB, 0);
	 
    lcd.init();
    lcd.backlight();
    lcd.clear();

	Serial.println("Connection au wifi ...");
	WiFi.begin(SSID, PSSW);

	while(WiFi.status() != WL_CONNECTED)
		delay(200);

	Serial.print("Connecte en ");  
	Serial.println(WiFi.localIP());

	delay(500);  

	Serial.print("Serveur a l'ecoute du port ");
	Serial.println(PORT, DEC);
	
	lcd.setCursor(0, 0);
    lcd.print(WiFi.localIP());
	lcd.setCursor(0, 1);
    lcd.print(PORT, DEC);

	webSocket.begin();
	webSocket.onEvent(webSocketEvent);
}

void loop()
{
	webSocket.loop();
}

void updateLed(String idLed, int intLed)
{
	char rgb;
	
	if (idLed == "RED:")
	{
		Serial.print("Led rouge eclairee a ");
		rgb = LEDRGBR;
	}
	else if (idLed == "GRN:")
	{
		Serial.print("Led verte eclairee a ");
		rgb = LEDRGBG;
	}
	else if (idLed == "BLE:")
	{
		Serial.print("Led bleue eclairee a ");
		rgb = LEDRGBB;
	}
	else
	{
		Serial.print(idLed);
		Serial.println(" --> message non traite");
		return;
	}

	Serial.print(intLed, DEC);
	Serial.println("%");
		
	analogWrite(rgb, map(intLed, 0, 100, 0, 255));
}

