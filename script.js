
$(

	function()
	{
		//var wsUri = "wss://echo.websocket.org/";
		var wsUri = "ws://192.168.0.164:7777/";
		var output;
		
		var redcursor = document.getElementById("redcursor");
		var greencursor = document.getElementById("greencursor");
		var bluecursor = document.getElementById("bluecursor");
		
		var redvalue = document.getElementById("redvalue");
		var greenvalue = document.getElementById("greenvalue");
		var bluevalue = document.getElementById("bluevalue");
		
		redvalue.innerHTML = redcursor.value; // Display the default slider value
		greenvalue.innerHTML = greencursor.value; // Display the default slider value
		bluevalue.innerHTML = bluecursor.value; // Display the default slider value

		// Update the current slider value (each time you drag the slider handle)
		redcursor.oninput = function()
		{
			redvalue.innerHTML = this.value;
			doSend("RED:"+this.value);
		}
		greencursor.oninput = function()
		{
			greenvalue.innerHTML = this.value;
			doSend("GRN:"+this.value);
		}
		bluecursor.oninput = function()
		{
			bluevalue.innerHTML = this.value;
			doSend("BLE:"+this.value);
		}
		
		$('#ouvre').click
		(
			function()
			{
				init();
			}
		)
		$('#envoi').click
		(
			function()
			{
				doSend("RED:50");
			}
		)
		$('#ferme').click
		(
			function()
			{
				websocket.close();
			}
		)

		function init()
		{
			output = document.getElementById("output");
			output.innerHTML = '';
			testWebSocket();
		}

		function testWebSocket()
		{
			websocket = new WebSocket(wsUri);
			websocket.onopen = function(evt) { onOpen(evt) };
			websocket.onclose = function(evt) { onClose(evt) };
			websocket.onmessage = function(evt) { onMessage(evt) };
			websocket.onerror = function(evt) { onError(evt) };
		}

		function onOpen(evt)
		{
			writeToScreen("CONNECTED");
			//doSend("WebSocket rocks");
		}

		function onClose(evt)
		{
			writeToScreen("DISCONNECTED");
		}

		function onMessage(evt)
		{
			writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
			//websocket.close();
		}

		function onError(evt)
		{
			writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
		}

		function doSend(message)
		{
			//writeToScreen("SEND: " + message);
			websocket.send(message);
		}

		function writeToScreen(message)
		{
			var pre = document.createElement("p");
			pre.style.wordWrap = "break-word";
			pre.innerHTML = message;
			output.appendChild(pre);
		}

		//window.addEventListener("load", init, false);
	}

);

